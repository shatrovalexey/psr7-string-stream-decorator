<?php
require_once( 'vendor/autoload.php' ) ;

use ashatrov\Psr7\StringStreamDecorator\StringStreamDecorator ;

/**
* StringStreamDecorator can receive as a resource and a string reference in the first argument
*/
// $inp = fopen( 'composer.json' , 'rb' ) ;
$inp = '{
    "name": "ashatrov/psr7-string-stream-decorator",
    "description": "PSR-7 decorator for StringStream",
    "license": "Private",
    "authors": [
        {
            "name": "Shatrov Aleksej",
            "email": "mail@ashatrov.ru"
        }
    ],
	"minimum-stability" : "dev" ,
	"version" : "1.0.5" ,
	"autoload" : {
		"psr-0" : {
			"ashatrov\\Psr7\\StringStreamDecorator" : "."
		} ,
		"classmap": [ "StringStreamDecorator.php" , "CipherFilter.php" ]
    }
}' ;
$out = fopen( 'out.txt' , 'wb' ) ;

$ssdh = new StringStreamDecorator( $inp ) ;
$encryption_key = $ssdh->encryption_key( ) ;

// Copy encrypted data into output resource handle
// $ssdh->encrypt( $out ) ;

// When encrypt has no aurguments then you can use it result as file handle
$out2 = $ssdh->encrypt( ) ;

while ( $data = fread( $out2 , 0x400 ) ) {
	// ... do something with encrypted data
}

$inp = fopen( 'out.txt' , 'rb' ) ;
$out = fopen( 'inp.txt' , 'wb' ) ;

$ssdh = new StringStreamDecorator( $inp , $encryption_key ) ;
$ssdh->decrypt( $out ) ;